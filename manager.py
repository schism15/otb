from __future__ import annotations
import threading
from queue import LifoQueue

from base import BaseMediator

class OtbManager(BaseMediator):
    def __init__(self, twitchio_bot: TwitchIOBot, otb_api: OtbApi) -> None:
        self._twitchio_bot = twitchio_bot
        self._twitchio_bot.mediator = self
        self._otb_api = otb_api
        self._otb_api.mediator = self

        q = LifoQueue()
        self._queue = q
        self._twitchio_bot._queue = q
        self._otb_api._queue = q

        self._twitchio_bot.start_bot()
        self._otb_api.start_app()
    # def notify(self, sender: object, event: str) -> None:
    #     if event['msg'] == 'payload_from_api_added_to_queue':
    #         self._twitchio_bot.fetch_message_from_api()