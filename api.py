import os
import time
import json
import threading

from flask import Flask, request

from base import BaseComponent

class OtbApi(BaseComponent):
    def __init__(self):
        app = Flask(__name__)

        @app.route('/commit', methods=['POST'])
        def queue_commit_msg():
            gitlab_auth_header = request.headers.get('X-Gitlab-Token')

            if gitlab_auth_header == os.environ['GITLAB_WEBHOOK_TOKEN']:
                # TODO: Wrap in try/except with logging
                # TODO: Adjust queue get on the other side to
                # account for multiple commits in one webhook
                # payload
                data = json.loads(
                    request.data,
                    strict=False
                )

                payload = {
                    'project': data['project']['name']
                    'comment': data['commits'][0]['title'],
                    'url': data['commits'][0]['url']
                }
                self._queue.put(payload)

                return {'success': True}
            else:
                # TODO: Log something about the token
                return {'success': False}

        self._app = app

    def start_app(self):
        x = threading.Thread(
            target=self._app.run,
            kwargs={
                'host': '0.0.0.0',
                'debug': False,
                'use_reloader': False
            }
        )
        x.start()
