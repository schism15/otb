import os
import time
import threading

from twitchio.ext import commands

from base import BaseComponent

class TwitchIOBot(commands.Bot, BaseComponent):

    def __init__(self):
        super().__init__(irc_token=os.environ['TMI_TOKEN'], client_id=os.environ['CLIENT_ID'],
            nick=os.environ['BOT_NICK'], prefix=os.environ['BOT_PREFIX'], initial_channels=[os.environ['CHANNEL']])

    # Events don't need decorators when subclassed
    async def event_ready(self):
        print(f'Ready | {self.nick}')
        ws = self._ws  # this is only needed to send messages within event_ready
        await ws.send_privmsg(os.environ['CHANNEL'], f"/me has landed!")
        
        # TODO: If possible break this out into its
        # own function, callable from the mediator
        
        while True:
            commit_info = self._queue.get()
            if commit_info:
                comment = commit_info['comment']
                url = commit_info['url']
                project = commit_info['project']
                await ws.send_privmsg(os.environ['CHANNEL'], f"Project: {project} | Commit: {comment} | {url}")

            time.sleep(1)

    # TODO: At the moment commands do not work
    # because of the loop workaround we are using in
    # event_ready. Once that is resolved, we can
    # we can bring this back.
    # Commands use a decorator...
    # @commands.command(name='test')
    # async def my_command(self, ctx):
    #     await ctx.send(f'Hello {ctx.author.name}!')
    
    def start_bot(self):
        x = threading.Thread(target=self.run, args=())
        x.start()