from bot import TwitchIOBot
from api import OtbApi
from manager import OtbManager

otb_api = OtbApi()
twitchio_bot = TwitchIOBot()

otb_manger = OtbManager(
    twitchio_bot = twitchio_bot,
    otb_api = otb_api
)